import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class DiceEvent implements EventHandler<ActionEvent> {
	private TextField balance;
	private TextField notice;
	private int choice;
	private TextField betAmount;
	private DiceGame dice;

	public DiceEvent(TextField balance, TextField notice, int choice, TextField betAmount, DiceGame dice) {
		this.balance = balance;
		this.notice = notice;
		this.choice = choice;
		this.betAmount = betAmount;
		this.dice = dice;
	}

	@Override
	public void handle(ActionEvent e) {
		int bet = Integer.parseInt(betAmount.getText());
		if (bet > dice.getBalance()) {
			notice.setText("not enough funds");
		}

		else {
			notice.setText(dice.playGame(choice, betAmount));
			balance.setText(dice.getBalance());
		}
	}
}
