/*
 * 
 * @author Antonio Simonelli
 */
public class Recursion {
	int count = 0;
	int i = 0;
	public int recursiveCount(String[] words, int n) {
		if(n > i) {
			return count;
		}
		for (i = 0; i <= words.length; i++) {
			
			if (i % 2 == 0) {
				for(int k = 0; k <= words[i].length(); k++) {
					if(words[i].charAt(k) == 'q') {
						count++;
						recursiveCount(words, n+1);
					}
				}
			}
		}
		return count;
		
	}

}
