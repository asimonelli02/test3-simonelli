import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DiceApplication extends Application {
	
	DiceGame game = new DiceGame();
	
	public void start(Stage stage) {
		Group root = new Group();
		HBox hbox = new HBox();
		VBox vbox = new VBox();
		TextField pMoney = new TextField();
		TextField betAmount = new TextField();
		Button small = new Button("Small");
		Button large = new Button("Large");
		TextField message = new TextField();
		
		hbox.getChildren().addAll(small, large);
		vbox.getChildren().addAll(pMoney, betAmount, hbox, message);
		root.getChildren().addAll(vbox);
		
		DiceEvent cSmall  = new DiceEvent(pMoney, message, 0, betAmount, game);
		DiceEvent cBig  = new DiceEvent(pMoney, message, 1, betAmount, game);
		
		small.setOnAction(cSmall);
		large.setOnAction(cBig);
		
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);

		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);

		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
